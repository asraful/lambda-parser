package project3c

import project3c.{ExprParser => parser}
import project3c.behaviors._

/**
 * Created by Asraful on 4/28/14.
 */
object REPL extends App{

  val prompt = "REPL> "

  val exprList = Array(
    "3",
    "x",
    "3 + 4 - 5",
    "  x + 3 :: y  ",
    "lambda x . x",
    "λ x . x",
    "λ x . x y z",
    "(x y)",
    "(x y z)",
    "1 :: nil",
    "1 :: 2 :: 3",
    "x :: y",
    "(x + 3) :: y",
    "head (1 :: 2)",
    "head (1 :: 2 :: 3 :: nil)",
    "head ((x % y) :: 2 :: 4 :: z)",
    "tail (1 :: 2)",
    "tail (1 :: 2 :: 3 :: nil)",
    "tail ((x % y) :: 2 :: 4 :: z)",
    "((1 + 2) - (3 * 4)) / 5",
    "lambda x . lambda y . x + y",
    "λ x1 x2 x3 x4 x5 x6 . x + y",
    "let " +
      "x1 = e1" + "\n" +
      "x2 = e2" + "\n" +
      "x3 = e3" + "\n" +
    "in e"
  )

  for( i <- 0 until exprList.length){
    val in = exprList(i).trim
    val out = getResult(in)
    println(prompt + in + "\n" + prompt + out + "\n")
  }

  def getResult(input: String): String = {
    eval(parser.parse(input).get) match {
      case Right(expr)  =>  PrettyPrinter.print(expr)
      case Left(msg)    =>  msg
    }
  }

}
