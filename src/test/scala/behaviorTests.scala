/**
 * Created by Asraful on 3/27/2014.
 */

package project3c

import org.scalatest.FunSuite

class behaviorTests extends FunSuite {

  import behaviors._
  import structures.ExprFactory._
  import project3c.{ExprParser => parser}


  /**
   * project3c tests
   */
  test("parsing correct expressions succeeds"){
    val parsed1           = parser.parse("3").get
    assert(parsed1       == constant(3))
    assert(eval(parsed1) == Right(constant(3)))

    val parsed2           = parser.parse("3 + 4").get
    assert(parsed2       == plus(constant(3), constant(4)))
    assert(eval(parsed2) == Right(constant(7)))

    val parsed3           = parser.parse("3 + 4 - 5").get
    assert(parsed3       == plus(constant(3), minus(constant(4), constant(5))))
    assert(eval(parsed3) == Right(constant(2)))

    val parsed4           = parser.parse("x + 3 :: y").get
    assert(parsed4       == plus(variable("x"), cell(constant(3), variable("y"))))
    assert(eval(parsed4) == Left("Error"))

    val parsed5           = parser.parse("x").get
    assert(parsed5       == variable("x"))
    assert(eval(parsed5) == Left("Error"))

    val parsed6           = parser.parse("lambda x . x").get
    assert(parsed6       == fun("x", variable("x")))
    assert(eval(parsed6) == Right(fun("x", variable("x"))))

    val parsed7           = parser.parse("λ x . x").get
    assert(parsed7       == fun("x", variable("x")))
    assert(eval(parsed7) == Right(fun("x", variable("x"))))

    val parsed8           = parser.parse("λ x . x y z").get
    assert(parsed8       == fun("x", app(app(variable("x"), variable("y")), variable("z"))))
    assert(eval(parsed8) == Right(fun("x", app(app(variable("x"), variable("y")), variable("z")))))

    val parsed9           = parser.parse("( x y )").get
    assert(parsed9       == app(variable("x"), variable("y")))
    assert(eval(parsed9) == Left("Error"))

    val parsed10           = parser.parse("( x y z )").get
    assert(parsed10       == app(app(variable("x"), variable("y")), variable("z")))
    assert(eval(parsed10) == Left("Error"))

    val parsed11           = parser.parse("( a b c d )").get
    assert(parsed11       == app(app(variable("a"), variable("b")), app(variable("c"), variable("d"))))
    assert(eval(parsed11) == Left("Error"))

    val parsed12           = parser.parse("if 0 then x else y").get
    assert(parsed12       == iff(constant(0), variable("x"), variable("y")))
    assert(eval(parsed12) == Left("Error"))

    val parsed13           = parser.parse("if x then y else z").get
    assert(parsed13       == iff(variable("x"), variable("y"), variable("z")))
    assert(eval(parsed13) == Left("Error"))

    val parsed14           = parser.parse("1 :: nil").get
    assert(parsed14       == cell(constant(1), constant(0)))
    assert(eval(parsed14) == Right(cell(constant(1), constant(0))))

    val parsed15           = parser.parse("1 :: 2").get
    assert(parsed15       == cell(constant(1), constant(2)))
    assert(eval(parsed15) == Right(cell(constant(1), constant(2))))

    val parsed16           = parser.parse("1 :: 2 :: 3").get
    assert(parsed16       == cell(constant(1), cell(constant(2), constant(3))))
    assert(eval(parsed16) == Right(cell(constant(1), cell(constant(2), constant(3)))))

    val parsed17           = parser.parse("x :: y").get
    assert(parsed17       == cell(variable("x"), variable("y")))
    assert(eval(parsed17) == Right(cell(variable("x"), variable("y"))))

    val parsed18           = parser.parse("( x + 3 ) :: y").get
    assert(parsed18       == cell(plus(variable("x"), constant(3)), variable("y")))
    assert(eval(parsed18) == Right(cell(plus(variable("x"), constant(3)), variable("y"))))

    val parsed19           = parser.parse("head ( 1 :: 2 )").get
    assert(parsed19       == constant(1))
    assert(eval(parsed19) == Right(constant(1)))

    val parsed20           = parser.parse("head ( 1 :: 2 :: 3 :: nil)").get
    assert(parsed20       == constant(1))
    assert(eval(parsed20) == Right(constant(1)))

    val parsed21           = parser.parse("head ( ( x % y ) :: 2 :: 4 :: z )").get
    assert(parsed21       == mod(variable("x"), variable("y")))
    assert(eval(parsed21) == Left("Error"))

    val parsed22           = parser.parse("head ( ( 1 :: 2 ) :: 3 :: 4 :: 5 )").get
    assert(parsed22       == cell(constant(1), constant(2)))
    assert(eval(parsed22) == Right(cell(constant(1), constant(2))))

    val parsed23           = parser.parse("tail ( 1 :: 2 )").get
    assert(parsed23       == constant(2))
    assert(eval(parsed23) == Right(constant(2)))

    val parsed24           = parser.parse("tail ( 1 :: 2 :: 3 :: nil)").get
    assert(parsed24       == cell(constant(2), cell(constant(3), constant(0))))
    assert(eval(parsed24) == Right(cell(constant(2), cell(constant(3), constant(0)))))

    val parsed25           = parser.parse("tail ( ( x % y ) :: 2 :: 4 :: z )").get
    assert(parsed25       == cell(constant(2), cell(constant(4), variable("z"))))
    assert(eval(parsed25) == Right(cell(constant(2), cell(constant(4), variable("z")))))

    val parsed26           = parser.parse("tail ( ( 1 :: 2 ) :: 3 :: 4 :: 5 )").get
    assert(parsed26       == cell(constant(3), cell(constant(4), constant(5))))
    assert(eval(parsed26) == Right(cell(constant(3), cell(constant(4), constant(5)))))

    val parsed27           = parser.parse("λ x1 x2 x3 x4 x5 x6 . x + y").get
    assert(parsed27       == fun("x1", fun("x2", fun("x3", fun("x4", fun("x5", fun("x6", plus(variable("x"), variable("y")))))))))
    assert(eval(parsed27) == Right(fun("x1", fun("x2", fun("x3", fun("x4", fun("x5", fun("x6", plus(variable("x"), variable("y"))))))))))

    val parsed28           = parser.parse("let x1 = e1 x2 = e2 x3 = e3 in e").get
    assert(parsed28       == app(fun("x3", app(fun("x2", app(fun("x1", variable("e")), variable("e1"))), variable("e2"))), variable("e3")))
    assert(eval(parsed28) == Left("Error"))

    val parsed29           = parser.parse("let x = 2 y = 3 z = 5 in x").get
    assert(parsed29       == app(fun("z", app(fun("y", app(fun("x", variable("x")), constant(2))), constant(3))), constant(5)))
    assert(eval(parsed29) == Right(constant(2)))

    val parsed30           = parser.parse("let x = 2 y = 3 z = 5 in x + y + z").get
    assert(parsed30       == app(fun("z", app(fun("y", app(fun("x", plus(variable("x"),plus(variable("y"),variable("z")))), constant(2))), constant(3))), constant(5)))
    assert(eval(parsed30) == Right(constant(10)))

    val parsed31           = parser.parse("let x = 2 y = 3 z = 5 in x + y + c").get
    assert(parsed31       == app(fun("z", app(fun("y", app(fun("x", plus(variable("x"),plus(variable("y"),variable("c")))), constant(2))), constant(3))), constant(5)))
    assert(eval(parsed31) == Left("Error"))

    val parsed32           = parser.parse("let x = 2 y = 3 in x + y + c").get
    assert(parsed32       == app(fun("y", app(fun("x", plus(variable("x"),plus(variable("y"),variable("c")))), constant(2))), constant(3)))
    assert(eval(parsed32) == Left("Error"))

    val parsed33           = parser.parse("let x = 2 y = 3 in x + y + 4").get
    assert(parsed33       == app(fun("y", app(fun("x", plus(variable("x"),plus(variable("y"),constant(4)))), constant(2))), constant(3)))
    assert(eval(parsed33) == Right(constant(9)))

    val parsed34          = parser.parse("let x = 2 y = 3 in x * y + 4").get
    assert(parsed34       == app(fun("y", app(fun("x", plus(times(variable("x"),variable("y")),constant(4))), constant(2))), constant(3)))
    assert(eval(parsed34) == Right(constant(10)))

    val parsed35          = parser.parse("let x = 2 y = 3 z= 4 in x * y + z").get
    assert(parsed35       == app(fun("z",app(fun("y", app(fun("x", plus(times(variable("x"),variable("y")),variable("z"))), constant(2))), constant(3))),constant(4)))
    assert(eval(parsed35) == Right(constant(10)))

    val parsed36          = parser.parse("let x = 2 y = 3 z= 4 in x * y + c").get
    assert(parsed36      == app(fun("z",app(fun("y", app(fun("x", plus(times(variable("x"),variable("y")),variable("c"))), constant(2))), constant(3))),constant(4)))
    assert(eval(parsed36) == Left("Error"))

  }


  test("parsing incorrect expressions fails"){
    assert(parser.parse("x y z").isEmpty        ==  true)
    assert(parser.parse("x λ z").isEmpty        ==  true)
    assert(parser.parse("head ( nil )").isEmpty ==  true)
  }



  /*
  /**
   * project3a tests
   */
  test("eval works") {

    assert(eval(expr3a1)  ==  Right(constant(3)))
    assert(eval(expr3a2)  ==  Left("Error"))
    assert(eval(expr3a3)  ==  Right(fun("x", plus(constant(3), variable("x")))))
    assert(eval(expr3a4)  ==  Right(constant(10)))
    assert(eval(expr3a5)  ==  Left("Error"))
    assert(eval(expr3a6)  ==  Right(constant(3)))
    assert(eval(expr3a7)  ==  Right(constant(4)))
    assert(eval(expr3a8)  ==  Right(constant(3)))
    assert(eval(expr3a9)  ==  Right(constant(3)))
    assert(eval(expr3a10) ==  Right(constant(120)))
    assert(eval(expr3a11) ==  Right(constant(20)))
    assert(eval(expr3a12) ==  Left("Error"))



    /**
    project3b tests.
     */
    assert(eval(expr3b1)  ==  Right(cell(plus(constant(3),constant(7)),minus(constant(5),constant(2)))))
    assert(eval(expr3b2)  ==  Right(constant(10)))
    assert(eval(expr3b3)  ==  Right(constant(3)))
    assert(eval(expr3b4)  ==  Right(constant(3)))
    assert(eval(expr3b5)  ==  Left("Error"))
    assert(eval(expr3b6)  ==  Left("Error"))
    assert(eval(expr3b7)  ==  Left("Error"))
    assert(eval(expr3b8)  ==  Right(fun("x",variable("x"))))


    assert(eval(app(app(Y, preLength), constant(0)))  ==  Right(constant(0)))
    assert(eval(app(app(Y, preLength), expr3b9))      ==  Right(constant(1)))
    assert(eval(app(app(Y, preLength), expr3b10))     ==  Right(constant(2)))
    assert(eval(app(app(Y, preLength), expr3b11))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preLength), expr3b12))     ==  Right(constant(4)))
    assert(eval(app(app(Y, preLength), expr3b13))     ==  Right(constant(1)))
    assert(eval(app(app(Y, preLength), expr3b14))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preLength), expr3b15))     ==  Right(constant(5)))


    assert(eval(app(app(Y, preSize), constant(0)))  ==  Right(constant(0)))
    assert(eval(app(app(Y, preSize), expr3b9))      ==  Right(constant(1)))
    assert(eval(app(app(Y, preSize), expr3b10))     ==  Right(constant(2)))
    assert(eval(app(app(Y, preSize), expr3b11))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preSize), expr3b12))     ==  Right(constant(4)))
    assert(eval(app(app(Y, preSize), expr3b13))     ==  Right(constant(1)))
    assert(eval(app(app(Y, preSize), expr3b14))     ==  Right(constant(6)))
    assert(eval(app(app(Y, preSize), expr3b15))     ==  Right(constant(5)))

  }
  */
}