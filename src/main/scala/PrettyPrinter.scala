package project3c

import project3c.structures._

/**
 * Created by Asraful on 4/28/14.
 */
object PrettyPrinter {
  def print(expr: Expr): String = {
    expr.out match {
      case Constant(value)  => value.toString
      case Var(name)    =>  name
      case Fun(x, e)    =>  "λ" + x + "." + (
        e.out match{
          case Fun(_, _)  =>  "(" + print(e) + ")"
          case _          =>  print(e)
        }
      )
      case App(l, r) =>  "(" + print(l) + "  " + print(r) + ")"

      case Plus(l, r)   =>  "(" + print(l) + " + " + print(r) + ")"
      case Minus(l, r)  =>  "(" + print(l) + " - " + print(r) + ")"
      case Times(l, r)  =>  "(" + print(l) + " * " + print(r) + ")"
      case Div(l, r)    =>  "(" + print(l) + " / " + print(r) + ")"
      case Mod(l, r)    =>  "(" + print(l) + " % " + print(r) + ")"
      case UMinus(x)    =>  "(" + "-" + print(x) + ")"

      case Cell(h, t)    =>  "(" + print(h) + " :: " + print(t) + ")"
    }
  }
}
