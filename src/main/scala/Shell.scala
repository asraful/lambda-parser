/**
 * Created by Asraful on 5/2/2014.
 */
package project3c

import java.io.{IOException, PrintWriter}

import jline._
import jline.console.ConsoleReader
import jline.TerminalFactory



object Shell {
  def main(args:Array[String]) {

    import behaviors._
    import project3c.{ExprParser => parser}

    try {

      var console = new ConsoleReader(System.in, System.out, new TerminalSupport(true) {});
      console.setPrompt("prompt> ");
      var line = "";

      var out = new PrintWriter(console.getTerminal().wrapOutIfNeeded(System.out));
      while ((line = console.readLine()) != null) {
        if(line == "exit"){ return}

        out.println(eval(parser.parse(line).get));
        out.flush()
      }
    } catch {
      case e: Exception => println("exception caught: " + e);
        e.printStackTrace();
    } finally {
      try {
        TerminalFactory.get().restore();
      } catch {
        case e: Exception => println("exception caught: " + e);
          e.printStackTrace();
      }
    }
  }

}