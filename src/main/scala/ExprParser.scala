package project3c

import project3c.structures._
import project3c.structures.ExprFactory._
import scala.util.parsing.combinator.syntactical.StandardTokenParsers

object ExprParser extends StandardTokenParsers {

  lexical.reserved   += ("lambda", "λ", "if", "then", "else", "nil", "head", "tail", "let", "in")
  lexical.delimiters += ("(", ")", "+", "-", "*", "/", "%", ".", "::", "=")

  def expr: Parser[Expr] =
    term ~ ("+" ~> expr) ^^ { case l ~ r => plus(l, r)  } |
    term ~ ("-" ~> expr) ^^ { case l ~ r => minus(l, r)  } |
    term ~ ("::" ~> expr) ^^ { case l ~ r => cell(l, r)  } |
    term

  def term: Parser[Expr] =
    factor ~ ("*" ~> term) ^^ { case l ~ r => times(l, r)  } |
    factor ~ ("/" ~> term) ^^ { case l ~ r => div(l, r)  } |
    factor ~ ("%" ~> term) ^^ { case l ~ r => mod(l, r)  } |
    factor


  def factor: Parser[Expr] = (
    numericLit ^^ { case s => constant(s.toInt) }
    | "nil" ^^ { case s =>  constant(0)}
    | "+" ~> factor ^^ { case e => e }
    | "-" ~> factor ^^ { case e => uminus(e) }
    | "λ" ~> rep(ident) ~ "." ~ exprs ^^ { case x ~ "." ~ e => x.length match{
        case 1 => fun(x(0), e)
        case _ =>
          val c = x.map(b => fun(b, e))
          x.init.reverse.foldLeft(c.reverse(0))(
            (r, v) => fun(v, r)
          )
      }}
    | "lambda" ~> rep(ident) ~ "." ~ exprs ^^ { case x ~ "." ~ e => x.length match{
        case 1 => fun(x(0), e)
        case _ =>
          val c = x.map(b => fun(b, e));
          x.init.reverse.foldLeft(c.reverse(0))(
            (r, v) => fun(v, r)
          )
      }}

      | "let" ~> rep1((ident <~ "=") ~ expr) ~ "in" ~ exprs ^^ { case x ~ "in" ~ e => x.length match{
      case 1 => app(fun(x.head._1, e), x.head._2)
      case _ =>
        val sp = app(fun(x.head._1, e), x.head._2)
        x.tail.foldLeft(sp){
          (l, r) => {
            app(fun(r._1, l), r._2)
          }
        }
    }}

    | ("if" ~> exprs) ~ ("then" ~> exprs) ~ ("else" ~> exprs) ^^ {  case cond ~ e1 ~ e2 => iff(cond, e1, e2)  }
    | "(" ~ exprs ~ ")" ^^ { case _ ~ e ~ _ => e }
    | ("head" ~ "(") ~> factor ~ ("::" ~> exprs) <~ ")" ^^ {  case h ~ t => h }
    | ("tail" ~ "(") ~> factor ~ ("::" ~> exprs) <~ ")" ^^ {  case h ~ t => t }
    | ident ^^ {  case (iden: String) => variable(iden) }

  )


  def exprs: Parser[Expr] =
    expr ~ expr ~ exprs ^^ { case e1 ~ e2 ~ e3  =>  app(app(e1, e2), e3)} |
    expr ~ expr ^^ { case e1 ~ e2  =>  app(e1, e2)} |
    expr


  def parseAll[T](p: Parser[T], in: String): ParseResult[T] =
    phrase(p)(new lexical.Scanner(in))

  def parse(in: String): ParseResult[Expr] = parseAll(expr, in)


}
