This is a parser or a front end (lexical and syntax analysis) to a Scala-based interpreter for the simple untyped lambda calculus. The Interpreter is also included with this project, but you will find [a standalone copy of the interpreter here](https://bitbucket.org/asraful/lambda-interpreter).


It also includes:

  - **Pretty Printer:** converts an abstract syntax tree back to concrete syntax in a readable format
  - **REPL:** A read–eval–print loop (REPL), also known as an interactive toplevel or language shell, is a simple, interactive computer programming environment that takes single user inputs (i.e. single expressions), evaluates them, and returns the result to the user




Grammer
=======

Here is a grammar in *EBNF*.

	expr ::= term { { "+" | "-" } term }*

	term ::= factor { { "*" | "/" } factor }*

	factor ::= "(" exprs ")"
         	| { "λ" | "lambda" } ident "." exprs
         	| "if" exprs "then" exprs "else" exprs
         	| "+" factor
         	| "-" factor
         	| numericLit
         	| ident

	exprs ::= expr+




Examples
=========

	test("parsing correct expressions succeeds") {
  		parser.parse("3").get assert_=== constant(3)
  		parser.parse("x").get assert_=== variable("x")
  		parser.parse("lambda x . x").get assert_=== fun("x", variable("x"))
  		parser.parse("lambda x . x y z").get assert_=== fun("x", app(app(variable("x"), variable("y")), variable("z")))
  		parser.parse("λ x . x").get assert_=== fun("x", variable("x"))
  		parser.parse("λ x . x y z").get assert_=== fun("x", app(app(variable("x"), variable("y")), variable("z")))
  		parser.parse("x y z").get assert_=== app(app(variable("x"), variable("y")), variable("z"))
	}

	test("parsing incorrect expressions fails") {
  		parser.parse("x λ z").isEmpty assert_=== true
	}
